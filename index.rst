.. |label| replace:: Kategorie-Listing
.. |snippet| replace:: FvCategoryListing
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.6.0
.. |maxVersion| replace:: 5.6.10
.. |Version| replace:: 1.2.0
.. |php| replace:: 7.4


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Das Plugin ermöglicht die Darstellung von Kategorie-Verlinkungen im „Kachel-Look“ oberhalb des Artikel-Listings. In der Kachel befindet sich das Kategorie-Bild und der Kategorie-Titel. Das Artikel-Listing kann flexibel Kategorie-spezifisch ein- oder ausgeblendet werden.

Frontend
--------
Wenn eine Kategorie angeklickt ist und diese hat Unterkategorien werden dies im "Kachel-Look" angezeigt.

.. image:: FvCategoryListing1.png

.. topic:: Hinweis!

    Auf den Herstellerseiten wird der "Kachel-Look" nicht angezeigt!
	

Backend
-------
Installation
____________
Nach der Installation muss sichergestellt sein, dass FvOLSI bereits das Kategorie-Album im Medienmanager erstellt hat.

.. image:: FvCategoryListing2.png

Sollte das Album noch nicht vorhanden sein, muss dies manuell erstellt werden. Die Bezeichnung des Albums muss „Kategorien“ sein.

In jedem Fall sollten die Thumbnail-Einstellungen überprüft werden:

Rechtsklick auf das Album => Eigenschaften:

.. image:: FvCategoryListing3.png

Dann öffnet sich folgendes Fenster:

.. image:: FvCategoryListing4.png

Thumbnails sollten hier aktiv sein und auch High-Resolution-Thumbnails werden vom Plugin unterstützt. Das Plugin greift automatisch die Thumbnail-Größe 0 ab, d.h. es werden in diesem Beispiel Bilder mit 200x200 Pixel Größe verwendet, was sich als geeignete Größe erwiesen hat.

Kategorien:
___________
Über die Kategorie-Freitextfelder kann das Plugin kategorie-spezifisch angepasst werden.

.. image:: FvCategoryListing5.png

:Title Background: Gibt an, welche Hintergrundfarbe das Titel-Overlay haben soll, wenn sie in einer Parent-Kategorie als Kachel dargestellt wird.
:Kacheln deaktivieren: wenn die Kacheln auf der Kategorieseite nicht erwünscht sind, können sie hier deaktiviert werden
:Artikel-Listing anzeigen: wenn der Haken gesetzt ist wird das Listing angezeigt 

technische Beschreibung
------------------------
keine

Modifizierte Template-Dateien
-----------------------------
:/listing/listing.tpl:



